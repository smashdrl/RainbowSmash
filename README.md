# Super Smash Bros. Ultimate (SSBU) × Deep Reinforcement Learning (Rainbow)

This is a project to make an AI agent that plays SSBU.
The agent is DeepMind's Rainbow [1].

Currently, all it can do is to train against in-game CPUs (incl. Amiibo) in the training mode.

## What you need to run this
Below are list of parts necessary for running this program. In paranthesis, I would list
devices that I'm using.
* A computer (M1 Mac Mini, 8GB RAM, 256GB SSD)
    * Decent CPU spec would be necessary.
    * It works on 8GB, but 16GB RAM may be preferred as it's close to the limit.
* Video capture card (Elgato HD60 S+)
    * Please prepare one that uses UVC protocol. Otherwise, OpenCV won't be able to read it.
    * Models with high-latency may not work very well.
* Nintendo Switch
    * It needs a dock, so Lite is not an option.
    * OLED model may work, but not tested.
    * Obviously, you also need SSBU Rom in this.
* USB virtual serial port adapter (DSD TECH SH-U09C5)
    * I recommend a device with an FTDI-chip. Others may work, but not tested.
    * If you use FTDI-chip on Windows or Linux, please follwo the instruction below to reduce the latency.
    * Make sure the voltage matches with your microcontroller. (For UNO R3, 5V)
* Microcontroller (Arduino UNO R3)
    * It will behave as a USB HID (Human Interface Device), so make sure yours can do.
    * If you want to use anything other than UNO R3, you'll need to compile a ROM image.


## Hardware set-up
I only tested this program on Apple Silicon M1 Macs. Please let me know if you could
make it work on Windows or Linux, so that I can modify the instruction.
That'll be a plus for the community.

### 1. Flash ROM to Arduino UNO R3
I made a flashable ROM image for Arduino UNO R3 in DFU Mode.
Please follow [this instruction](https://github.com/javmarina/Nintendo-Switch-Remote-Control/blob/master/firmware/README.md#arduino-uno-r3) to flash the ROM located at Arduino/Joystick.hex
into your Arduino UNO R3.

I only prepared it for Arduino UNO R3, but it may work with other microcontrollers.
If you want to make your own ROM, please go to the
[Nintendo Switch Remote Control](https://github.com/javmarina/Nintendo-Switch-Remote-Control)
repo and follow the instruction. (Make sure you match the baud rate for serial port
connection between your microcontroller and rainbow/client.py. The default in client.py
is 57600 Hz. (This is the largest number that worked on my R3. 1M Hz didn't work.))
(Also, I couldn't compile the ROM on Mac, so I used Linux just for that. You can still
Flash the ROM on your Mac.)

### 2. Get USB virtual serial port device ready
I recommend using FTDI-chip-based USB serial port adapter.

Go to [FTDI's page](https://ftdichip.com/drivers/vcp-drivers/) and get/install the
driver for your platform.

Also, please follow [this instruction](https://projectgus.com/2011/10/notes-on-ftdi-latency-with-arduino/) to reduce the latecy timer if you are on Windows or Linux.

Once your device is ready, connect the device to your microcontroller (UNO R3), following
[this instruction](https://github.com/javmarina/Nintendo-Switch-Remote-Control/blob/master/gui/README.md). Briefly,
1. Connect following pins of the serial adapter and UNO R3, respectively,
    * RX -> RX
    * TX -> TX
    * VCC -> 5V
    * GND -> GND
2. Connect serial adapter to the host computer
3. Connect UNO R3 to Nintendo Switch dock's USB port

### 3. Get video capture card ready
Connect your capture card to your host computer, install driver software. Connect HDMI
out from Switch to its imput. Though not essential, if you have an extra monitor to pass
through the signal, it is convenient for diagnosis.

## Software set-up
### 1. Install python environment
If you are using M1 Mac, probably PyTorch that is compiled natively for M1 is important,
so please install it first. I recommend miniforge conda for doing so.

    brew install miniforge
    
(If you don't have Homebrew, install it from [here](https://brew.sh).)

Next, let's make a virtual environment and install pytorch.

    conda create --name pytorch_env python=3.8
    conda activate pytorch_env
    conda install -c pytorch pytorch torchvision
    
If you are using Linux, x64 (or amd64) binary would be fine. For Windows, please find an
instruction for installing PyTorch.

Next, install all the other requirements for this package.

    pip install -r requirements.txt

### 2. Install VSCode
Although this is not mandatory, run configurations are set up in VSCode, so it might be
convenient to use the same thing.
Install it [from here](https://code.visualstudio.com), and install python-related plugins.


### 3. Set up USB serial port adapter
First, identify the path of your device.
If you are using a Mac try the following command in a command line,

    ls /dev/cu*

You'll see a list of serial port devices on your computer. Find the one for your USB
device. (If you are not sure which one, you can try to disconnect/connect your device
and check which one disappear/appear here.) In my case, it is '/dev/cu.usbserial-4'.

Once you identified this, write it into your rainbow/client.py line 589 where it specifies
the serial port device path.

### 4. Set up video capture
Again, identify the video capture device that OpenCV identifies. In my case, Mac mini
doesn't have any webcam, so the device number is 0.
If you use other devices (like MacBook Air), you'll need to identify the
device number.

First, just try running env.py (of course in the virtual env you just set up).

    python rainbow/env.py

You'll see a small window that shows the video input.
If this window shows a 84x84 grayscale version of the game screen, you are good.
If not, try next video device by changing line 123 of env.py, where it says

    self.video = cv2.VideoCapture(0)  # try the next number if 0 doesn't work

Once you get the video input right, you are ready to start.

### 5. (Mac only) Install Caffeine
Install Caffeine [from here](https://www.macupdate.com/app/mac/24120/caffeine). This is
necessary to keep the performance of Mac constant. If you don't install it, the program
will still run, but it starts dropping a lot of video frames.

### 6. Settings on Switch
1. Change the output resolution of Switch to 720p, adjust screen size to 100%.
2. If you have changed any other video-related settings in SSBU or Switch, revert the changes.
3. Make sure that the Pro-controller can be used in the wired mode
4. Launch SSBU, go to Key Config. Make the following config and save it.
    * Button A: Attack
    * Button B: Special attack
    * Button Y: Grab
    * Button X: Jump
    * Button ZL: Sheild
    * In Other setting, Stick Jump: Off
    * In Other setting, Stick Sensitivity: High

## How to run the reinforcement learning program
### 1. Do preps in the training mode
1. Go into the training mode
2. Choose a stage (I recommend FD Electroplancton universe for its simplicity)
3. Choose fighter you want to train in P1, and anotehr fighter for CPU (I'm doing Incineroar against Marth, but this is really arbitrary. Just choose what your want to see. However,
if the fighter has ability to recover %, it may cause some issues in a kill judgement.)
4. Set the level of CPU as desired (I recommend gradually increasing from 1).
5. Start the training mode
6. Open settings and set up as follows
    * No. of CPUs: 1
    * CPU Damage %: **12**
    * Fixed Damage: Off
    * Trajectory Guide: Off
    * CPU Behavior: **CPU (Lv. X)**
    * Speed: x1
    * Combo: **Do Not Display**
    * CPU Shuffling: **A lot**
    * Stale Moves: **On**
    * P1 Damage %: **12**
7. Close the setting window

### 2. Register the controller on microcontroller
1. In the training mode, turn off the Switch controller that you are using by pressing
the power button(s). (For Pro
Controller, its a small button at the top. For JoyCons, they are small buttons located
on the side that connects to the dock.)
2. Wait for a few seconds until you see a screen that instructs you to choose a fighter.
3. Run env.py with 'setup' option 'on'. (Don't worry. This is default. You can run this
either through VSCode or directly in the terminal. You may have to be on the project
top directry to run it.)
4. Confirm that the controller in your microcontroller is able to choose the fighter,
return to training mode, and start moving the fighter randomly.
5. Stop env.py by pressing Ctrl+C in terminal.

### 3. Run the main program
1. In VSCode, open and select main.py in the editor pane.
2. In VSCode, choose Run and Debug from the side pane.
3. To start a new training session, Choose 'Rainbow canonical (or recurrent if you'd like) reset' and run from
Menu -> Run -> Run Without Debugging.
4. To continue training from the previous run, Choose 'Rainbow canonical (recurrent) cont' and run
from Menu -> Run -> Run Without Debugging.

Note: If you run with the green play button near the configuration dropdown menu, you
are running it with debugger. Since the debugger drops the performance of the program,
run from the menu bar without the debugger.

### 4. How to deal with trained models
* Trained network is stored in results/default/.
* Memory file is stored in memory1.
* I prepared a simple shell script for saving the model. Please run this

        ./savemodel.sh <name>
        
    then your model would be saved in results/\<name\>/
* To recover the saved model, overwrite results/default/model.pth and memory1 with your
saved files.
* plot_csvdata.py is a handy script for plotting reward/Q/Loss/Episode_Length. Just run
it in the project top directory. It'll generate some plots as files. (It'll try to plot
continuosuly every minute. Stop it by Ctrl-C if you just want to generate figure once.)

## Acknowledgement
This project is based on the following project. I appreciate their original contributions.
* [PyTorch implementation of Rainbow](https://github.com/Kaixhin/Rainbow)
* [Nintendo Switch Remote Control](https://github.com/javmarina/Nintendo-Switch-Remote-Control)

## References
[1] Rainbow: Combining Improvements in Deep Reinforcement Learning <https://arxiv.org/abs/1710.02298>