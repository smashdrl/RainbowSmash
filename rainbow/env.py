# -*- coding: utf-8 -*-
from collections import deque
import random
import cv2
import torch
import numpy as np
import client
import time
import pandas as pd
import logging
from torch.nn import functional as F
from pathlib import Path

episode_buffer = 20000  # I rarely see longer than this.

vsize = 84 * 1
point_position1 = (282, 654)
point_position2 = (554, 654)

frame_clip_normal = (0.045, 0.059)
frame_clip_quick = (0.023, 0.037)
clip_margin = 0.003

# input_latency = 6

buffer_length = 7
d_buffer1 = deque([510] * buffer_length, maxlen=buffer_length)
d_buffer2 = deque([510] * buffer_length, maxlen=buffer_length)
accepted_damages = [1, 0]
colormap = pd.read_csv("damage_color_map.csv", index_col=0)
colormap_list = []


logging.basicConfig(
    filename="envlog.txt",
    filemode="w",
    level=logging.INFO,
    format="%(asctime)s %(message)s",
)

for i in colormap.iterrows():
    colormap_list.append(list(i[1]))

# input_latency = 6

buffer_length = 7
d_buffer1 = deque([510] * buffer_length, maxlen=buffer_length)
d_buffer2 = deque([510] * buffer_length, maxlen=buffer_length)
accepted_damages = [1, 0]
colormap = pd.read_csv("damage_color_map.csv", index_col=0)
colormap_list = []


logging.basicConfig(
    filename="envlog.txt",
    filemode="w",
    level=logging.INFO,
    format="%(asctime)s %(message)s",
)

for i in colormap.iterrows():
    colormap_list.append(list(i[1]))


class Clock:
    def __init__(self, fps):
        self.start = time.perf_counter()
        self.frame_length = 1 / fps

    @property
    def tick(self):
        return int((time.perf_counter() - self.start) / self.frame_length)

    def sleep(self):
        r = self.tick + 1
        while self.tick < r:
            time.sleep(1 / 1000)


def process_damage(
    point, prev_damage, unsure_dur, d_buffer, colormap_list, suicide_punishment
):
    # damage is defined as Red + Green. Original order is BGR, so that's [1] and [2].
    rg = int(point[1]) + int(point[2])
    d_buffer.append(rg)
    new_damage = prev_damage
    reward = 0
    killed = False
    new_unsure = unsure_dur + 1
    # if (d_buffer[2] == d_buffer[1]) & (d_buffer[2] == d_buffer[0]):
    if d_buffer.count(rg) == len(d_buffer):  # if all the same
        # stable for several frames. accept
        exp_color = colormap_list[rg]  # expected color in rgb
        cdiff = np.sqrt((exp_color[0] - point[2]) ** 2 + (exp_color[1] - point[1]) ** 2)
        if (cdiff < 10) & (d_buffer[-1] > 119):  # finally accept
            # R+G less than 120 will automatically be rejected, since it never express
            # damage %.
            new_unsure = 0
            new_damage = 510 - d_buffer[-1]
            # special case for kill
            if (new_damage < prev_damage) & (new_damage == 0):  # killed
                reward = 510 - prev_damage
                killed = True
                # Note that this could be problematic if the fighter has an ability to recover %
                # TODO: Write remedy for that (check time or something)
                # also, if the fighter dies at 0% damage, it will also be weird.
                # TODO: fix that, too.
            elif (
                (unsure_dur > 25)
                & (unsure_dur < 60)
                & (new_damage == 0)
                & (prev_damage == 0)
            ):
                # presumably, died with 0 damage
                reward = 510 + 255 * suicide_punishment
                killed = True
                # print(f"damage 0 unsure_kill! unsure_dur: {unsure_dur}")
                # logging.info(f"damage 0 unsure_kill! unsure_dur: {unsure_dur}")
                logging.info(f"Zero damage kill. Damage unsure duration: {unsure_dur}")
            else:
                reward = new_damage - prev_damage
    return (new_damage, reward, killed, new_unsure, unsure_dur)


class Env:
    prev_damage1 = 0
    prev_damage2 = 0
    unsure_duration1 = 0
    unsure_duration2 = 0
    prev_time = 0
    steps = 0

    def __init__(self, args):
        self.device = args.device
        self.input_latency = args.input_latency
        self.num_stocks = args.num_stocks
        self.stock1 = self.num_stocks
        self.stock2 = self.num_stocks
        self.quick_mode = args.quick_mode
        self.suicide_punishment = args.suicide_punishment
        if args.quick_mode:
            self.frame_clip = frame_clip_quick
        else:
            self.frame_clip = frame_clip_normal
        actions = self.get_action_set()
        # actions = self.ale.getMinimalActionSet()
        # self.clock = Clock(20.1)
        self.dropped = 0

        self.actions = dict([i, e] for i, e in zip(range(len(actions)), actions))
        self.life_termination = (
            False  # Used to check if resetting only from loss of life
        )
        self.window = args.history_length  # Number of frames to concatenate
        self.state_buffer = deque([], maxlen=args.history_length)
        if self.window == 1:  # history = 1 (recurrent)
            self.action_buffer = deque([], maxlen=args.history_length)
        else:
            self.action_buffer = deque([], maxlen=args.history_length - 1)
        self.training = True  # Consistent with model training mode
        self.video = cv2.VideoCapture(0)  # set the right number
        self.render = args.render
        self.all_reward = np.zeros(episode_buffer)
        self.prevtime = 0
        self.prev_action = 0

    def get_action_set(self):
        # tentatively, 9 * 6 = 54 actions
        return range(9 * 6)

    def send_action(self, action):
        # get the cursor key
        button = action // 9
        stick = action % 9
        # TODO action will be sent here
        cmd = client.BTN_GROUP[button]
        cmd += client.LSTICK_GROUP[stick]
        return client.send_cmd(cmd, just_send=True)

    def _get_state(self):
        done = False
        # self.clock.sleep()
        # get 3 images (and trash two of them)
        success, img = self.video.read()
        self.curtime = time.perf_counter()
        tdiff = self.curtime - self.prevtime
        while tdiff < self.frame_clip[0] - clip_margin:
            success, img = self.video.read()
            self.curtime = time.perf_counter()
            tdiff = self.curtime - self.prevtime
        if (tdiff > self.frame_clip[1] + clip_margin) & (tdiff < 0.5):
            # print(f"tdiff is {tdiff}. a frame might be skipped")
            self.dropped += 1

        while not success:
            print("screen turning on... process being protected")
            success, img = self.video.read()

        self.prevtime = self.curtime
        # while img.__class__ != np.ndarray:  # would this fix the issue?
        # here, insert a part for getting damage on both sides
        # TODO calculate reward
        # print(frame[point_position1[1], point_position1[0], :])
        # print(frame[point_position2[1], point_position2[0], :])
        p1 = img[point_position1[1], point_position1[0], :]  # bgr
        p2 = img[point_position2[1], point_position2[0], :]
        # damages = (int(p1[1]) + p1[2], int(p2[1]) + p2[2])
        # damages = (p1, p2)
        # damage_candidate1
        # TODO write schemes to accept damage
        (
            self.prev_damage1,
            reward_1,
            killed_1,
            self.unsure_duration1,
            rollback1,
        ) = process_damage(
            p1,
            self.prev_damage1,
            self.unsure_duration1,
            d_buffer1,
            colormap_list,
            self.suicide_punishment,
        )
        # there is no suicide punishment for player 2
        (
            self.prev_damage2,
            reward_2,
            killed_2,
            self.unsure_duration2,
            rollback2,
        ) = process_damage(
            p2, self.prev_damage2, self.unsure_duration2, d_buffer2, colormap_list, 0.0
        )
        rollback1 += self.input_latency
        rollback2 += self.input_latency
        # assert reward_1 >= 0
        # assert reward_2 >= 0
        if reward_1 < 0:
            logging.warning(f"reward_1 is negative!: {reward_1}\n")
        if reward_2 < 0:
            logging.warning(f"reward_2 is negative!: {reward_2}\n")

        reward = reward_2 - reward_1
        if reward_1 != 0:
            self.all_reward[self.steps - rollback1] -= reward_1 / 255.0
        if reward_2 != 0:
            self.all_reward[self.steps - rollback2] += reward_2 / 255.0

        # remove stock if killed
        if killed_1:
            self.stock1 -= 1
            logging.info(f"p1 killed! remaining stocks: {self.stock1}")
        if killed_2:
            self.stock2 -= 1
            logging.info(f"p2 killed! remaining stocks: {self.stock2}")

        if self.stock1 == 0:
            # one stock 2 points.
            # perfect 3-stock game is 6 points
            reward -= 0  # you lose. big punishment
            self.all_reward[self.steps - rollback1] -= 0.0
            done = True
        if self.stock2 == 0:
            reward += 0  # you win.
            self.all_reward[self.steps - rollback2] += 0.0
            done = True

        # if reward != 0:
        #    print(f"reward1: {reward_1:3d}, reward2: {reward_2:3d}")

        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        state = cv2.resize(gray, (vsize, vsize), interpolation=cv2.INTER_LINEAR)

        return (
            torch.tensor(state, dtype=torch.float32, device=self.device).div_(255),
            reward / 255.0,
            done,
        )

    def _reset_buffer(self):
        for _ in range(self.window):
            self.state_buffer.append(torch.zeros(vsize, vsize, device=self.device))
            self.action_buffer.append(
                torch.zeros(self.action_space(), device=self.device)
            )

    def action_to_onehot(self, action):
        a = torch.zeros(self.action_space(), device=self.device)
        a[action] = 1.0
        return a
        # return F.one_hot(action, num_classes=self.action_space).float()

    def reset(self):
        client.send_cmd(client.TRAINING_RESET)
        client.p_wait(0.5)
        self.stock1 = self.num_stocks
        self.stock2 = self.num_stocks
        self.prev_damage1 = 0
        self.prev_damage2 = 0
        self.unsure_duration1 = 0
        self.unsure_duration2 = 0
        self.steps = 0
        self.all_reward = np.zeros(episode_buffer)
        d_buffer1 = deque([510] * buffer_length, maxlen=buffer_length)
        d_buffer2 = deque([510] * buffer_length, maxlen=buffer_length)
        print(f"{self.dropped} frames might be dropped.")
        self.dropped = 0
        if self.life_termination:
            self.life_termination = False  # Reset flag
            # self.ale.act(0)  # Use a no-op after loss of life
        else:
            # Reset internals
            self._reset_buffer()
            # self.ale.reset_game()
            # Perform up to 30 random no-ops before starting
            # for _ in range(random.randrange(30)):
            # self.ale.act(0)  # Assumes raw action 0 is always no-op
            # if self.ale.game_over():
            #    self.ale.reset_game()
        # Process and return "initial" state
        observation, _, _ = self._get_state()
        self.state_buffer.append(observation)
        self.action_buffer.append(self.action_to_onehot(0))
        s = torch.stack(list(self.state_buffer), 0)
        alist = list(self.action_buffer)
        if self.window > 1:
            alist.append(self.action_to_onehot(0))
        a = torch.stack(alist, 0)
        return (s, a)

    def get_reward(self):
        # TODO write actual reward
        return 1

    def step(self, action):
        # new 20 hz code
        self.send_action(action)
        state, reward, done = self._get_state()
        self.state_buffer.append(state)
        self.action_buffer.append(self.action_to_onehot(action))
        if self.render:
            cv2.imshow("state", np.array(state))
            cv2.waitKey(1)
        # ensure 20Hz
        # current_time = time.time()
        # elap = current_time - self.prev_time
        # # print(elap)
        # if elap < 0.0460:
        #     # client.p_wait(0.05 - elap - 0.000)
        #     time.sleep(0.0460 - elap)
        # self.prev_time = time.time()

        self.steps += 1
        if done:
            all_reward = self.all_reward[: self.steps]
            logging.info(f"Episode done. Total steps: {self.steps}")
            self.steps = 0
        else:
            all_reward = None

        s = torch.stack(list(self.state_buffer), 0)

        alist = list(self.action_buffer)
        if self.window > 1:
            alist.append(self.action_to_onehot(0))
        a = torch.stack(alist, 0)

        return (s, a), reward, done, all_reward

    def step_old(self, action):
        # Repeat action 4 times, max pool over last 2 frames
        frame_buffer = torch.zeros(2, vsize, vsize, device=self.device)
        reward_tot, done = 0, False
        for t in range(2):
            self.send_action(action)
            state, reward, done = self._get_state()
            reward_tot += reward
            if t == 0:
                # frame_buffer[0] = self._get_state()
                frame_buffer[0] = state
            elif t == 1:
                # frame_buffer[1] = self._get_state()
                frame_buffer[1] = state
            # done = self.ale.game_over()
            if done:
                break
        observation = frame_buffer.max(0)[0]
        self.state_buffer.append(observation)
        cv2.imshow("state", np.array(observation))
        return torch.stack(list(self.state_buffer), 0), reward_tot, done

    # Uses loss of life as terminal signal
    def train(self):
        self.training = True

    # Uses standard terminal signal
    def eval(self):
        self.training = False

    def action_space(self):
        return len(self.actions)

    # def render(self):
    #    # cv2.imshow("screen", self.ale.getScreenRGB()[:, :, ::-1])
    #    cv2.imshow("screen", self.ale.getScreenRGB()[:, :, ::-1])

    def close(self):
        cv2.destroyAllWindows()


class Fake:
    def __init__(self):
        self.device = torch.device("cpu")
        self.history_length = 4
        self.input_latency = 6
        self.quick_mode = False
        self.num_stocks = 1
        self.suicide_punishment = 0
        return


def setup_controller():
    client.send_cmd(client.BTN_A)
    client.p_wait(0.5)
    client.send_cmd(client.BTN_L + client.BTN_R)
    client.p_wait(2)

    # client.send_cmd(client.LSTICK_U_L)
    client.send_cmd(client.LSTICK_D_L)
    client.p_wait(0.5)
    client.send_cmd(client.BTN_A)
    client.p_wait(1)
    client.send_cmd(client.BTN_PLUS)
    client.p_wait(1)
    return


def test_step(render=False, do_setup=True):
    args = Fake()
    args.device = torch.device("cpu")
    args.history_length = 4
    args.render = render
    env = Env(args)
    tot_reward = 0

    stime = time.time()
    counter = 0
    if do_setup:
        setup_controller()
    while True:
        random_action = np.random.randint(54)
        state, reward, done, _ = env.step(random_action)
        tot_reward += reward
        if reward != 0:
            print(f"{reward}, {tot_reward}")
        if done:
            tot_reward = 0
            env.reset()
            client.p_wait(1)
        now = time.time()
        counter += 1
        if now - stime > 1.0:
            print(f"{counter} commands per second")
            counter = 0
            stime = now


def test_video_interval():
    args = Fake()
    args.device = torch.device("cpu")
    args.history_length = 4
    args.render = False
    env = Env(args)
    tot_reward = 0

    stime = time.time()
    counter = 0
    tdiff_samples = []
    while True:
        success, img = env.video.read()
        now = time.time()
        tdiff = now - stime
        if counter > 10:
            tdiff_samples.append(tdiff)
        if counter > 110:
            break
        counter += 1
        stime = now
    print("Time difference between frames from your video capture card:")
    print(tdiff_samples)
    tdiffs = np.array(tdiff_samples)
    tdiff2 = np.convolve(tdiffs, [1] * 2, mode="valid")
    tdiff3 = np.convolve(tdiffs, [1] * 3, mode="valid")

    # for 20fps settings, do this (default)
    print(f"Normal Mode (20fps) settings: [{min(tdiff3):.3f}, {max(tdiff3):.3f}]")
    print(f"Quick  Mode (30fps) settings: [{min(tdiff2):.3f}, {max(tdiff2):.3f}]")
    print(f"Full   FPS  (60fps) settings: [{min(tdiffs):.3f}, {max(tdiffs):.3f}]")


def test_input_latency(do_setup=False, quick_mode=False):
    args = Fake()
    args.device = torch.device("cpu")
    args.history_length = 4
    args.render = False
    args.quick_mode = quick_mode
    env = Env(args)
    tot_reward = 0

    stime = time.time()
    counter = 0
    image_buffer = []
    if do_setup:
        setup_controller()
    while True:
        # random_action = np.random.randint(54)
        if (counter % 60) == 0:
            action = 45  # stick: neutral, button: ? Shield?
        else:
            action = 0
        state, reward, done, _ = env.step(action)
        # env.send_action(action)
        # state, reward, done = env._get_state()
        image_buffer.append(np.array(state[0][0, :, :] * 255))

        tot_reward += reward
        if reward != 0:
            print(f"{reward}, {tot_reward}")
        # cv2.imshow("state", np.array(state[0, :, :]))
        # image, damage = env._get_state()
        if done:
            tot_reward = 0
            env.reset()
            client.p_wait(1)
        now = time.time()
        counter += 1
        if now - stime > 5.0:
            print(f"{counter} total commands in 5 secs")
            counter = 0
            stime = now
            print("Writing images...")
            Path("images").mkdir(exist_ok=True)
            for i in range(len(image_buffer)):
                cv2.imwrite(f"images/{i}.png", image_buffer[i])
            print("Done.")
            return

        key = cv2.waitKey(1)
        if key == ord("q"):
            break


def test_control():
    args = Fake()
    args.device = torch.device("cpu")
    args.history_length = 4
    # env = Env(args)
    tot_reward = 0

    stime = time.time()
    counter = 0
    setup = True
    if not setup:
        setup_controller()

    while True:
        random_action = np.random.randint(54)
        # state, reward, done = env.step(random_action)
        # tot_reward += reward
        # print(f"{reward}, {tot_reward}")
        # cv2.imshow("state", np.array(state[0, :, :]))
        # image, damage = env._get_state()
        action = random_action
        button = action // 9
        stick = action % 9
        print(f"B: {button}")
        print(f"S: {stick}")
        cmd = client.BTN_GROUP[button]
        cmd += client.LSTICK_GROUP[stick]
        client.send_cmd(cmd)
        now = time.time()
        counter += 1
        if now - stime > 1.0:
            print(f"{counter} commands per second")
            print(f"time {now}")
            counter = 0
            stime = now

        # f.write(f"{t},{damage[0]},{damage[1]}\n")
        # f.write(f"{t},{damage[1][0]},{damage[1][1]},{damage[1][2]}\n")
        # f.write(f"{t},{damage}\n")
        # if damage != 0:
        #    print(damage)
        # cv2.imshow("state", np.array(image))
        key = cv2.waitKey(1)
        if key == ord("q"):
            break
    # f.close()

    return


def test_env():
    args = Fake()
    args.device = torch.device("cpu")
    args.history_length = 4
    args.render = False
    env = Env(args)
    tot_reward = 0

    f = open("damage_result_real.csv", "w")
    # f.write("time,damage1,damage2\n")
    f.write("time,b,g,r\n")
    stime = time.time()
    ptime = time.perf_counter()
    difflist = []
    timelist = []
    while True:
        random_action = np.random.randint(54)
        s = time.perf_counter()
        ret = env.send_action(random_action)
        e = time.perf_counter()
        curtime = time.perf_counter()
        # print(f"interval : {curtime - ptime}")
        difflist.append(curtime - ptime)
        timelist.append(e)
        ptime = curtime
        if len(difflist) % 100 == 0:
            print(f"mean:{np.mean(difflist)}, std:{np.std(difflist)}")
            diffs = np.diff(timelist[0::4])
            print(f"3diff:{diffs.min()} - {diffs.max()}")
            # print([int(d * 1000) for d in difflist])
            difflist = []
            timelist = []
        if not ret:
            print("command failed")
        # s = time.perf_counter()
        # time.sleep(0.001)
        # e = time.perf_counter()
        # print(f"interval: {e - s}")
        image, damage, done = env._get_state()
        # t = time.time() - stime
        # f.write(f"{t},{damage}\n")
        # if damage != 0:
        # print(damage)
        # cv2.imshow("state", np.array(image))
        # key = cv2.waitKey(1)
        # if key == ord("q"):
        #    break
    f.close()

    return


if __name__ == "__main__":
    test_video_interval()
    # test_env()
    # test_control()
    test_input_latency(do_setup=True)
    test_step(render=True, do_setup=False)
    print("test done")
