import unittest
import torch

from env_orig import Env
from env_orig import Fake
from agent import Agent
from memory import ReplayMemory


class EnvTestCase(unittest.TestCase):
    def setUp(self):
        args = Fake()
        args.device = torch.device("cpu")
        args.history_length = 4
        args.render = False
        args.seed = 0
        args.max_episode_length = 1e6
        args.game = "space_invaders"
        self.env = Env(args)

        args.atoms = 51
        args.V_min = -1
        args.V_max = 1
        args.batch_size = 32
        args.multi_step = 4
        args.discount = 0.99
        args.norm_clip = 10
        args.model = None
        args.architecture = "canonical"
        args.hidden_size = 256
        args.noisy_std = 0.1
        args.learning_rate = 6.25e-5
        args.adam_eps = 1.5e-4
        self.dqn = Agent(args, self.env)

        args.memory_capacity = 10000
        args.priority_weight = 0.4
        args.priority_exponent = 0.5
        action_space = self.env.action_space()
        self.mem = ReplayMemory(args, args.memory_capacity, action_space)

    def test_step(self):
        self.env.reset()
        state, reward, done, all_reward = self.env.step(0)
        action, Q = self.dqn.act(state)
        return self.env.step(action)
