# %%
# just playing with recurrent network

import torch
from torch import nn
from torch.nn import functional as F

from tqdm import trange

hidden_size = 256


class Net(nn.Module):
    def __init__(self):
        super(Net, self).__init__()
        self.convs = nn.Sequential(
            nn.Conv2d(1, 32, 8, stride=4, padding=0),
            nn.SELU(),
            nn.Conv2d(32, 64, 4, stride=2, padding=0),
            nn.SELU(),
            nn.Conv2d(64, 64, 3, stride=1, padding=0),
            nn.SELU(),
        )
        self.conv_output_size = 3136

        self.fc1 = nn.Linear(self.conv_output_size, hidden_size)
        self.lstm = nn.LSTM(hidden_size, hidden_size, num_layers=2)
        self.fc2 = nn.Linear(hidden_size, 1)
        self.lstm_state = (
            torch.zeros(2, 1, hidden_size),
            torch.zeros(2, 1, hidden_size),
        )

    def forward(self, x, log=False):
        x = self.convs(x)
        x = x.view(-1, self.conv_output_size)
        x = self.fc1(x)
        x = F.selu(x).unsqueeze(0)
        x, self.lstm_state = self.lstm(x, self.lstm_state)
        x = F.selu(x)
        x = self.fc2(x)
        return x


net = Net()

img = torch.rand((1, 1, 84, 84))
# for i in range(10):
#     a = net(img)
#     print(a)

for i in trange(1000):
    img = torch.rand((1, 1, 84, 84))
    a = net(img)

# %% playing with memory

from memory import ReplayMemory

args = Net()
args.memory_capacity = 10
args.action_space = 5
args.device = torch.device("cpu")
args.history_length = 4
args.discount = 0.99
args.multi_step = 4
args.priority_weight = 0.4
args.priority_exponent = 0.5


mem = ReplayMemory(args, args.memory_capacity, args.action_space)

for i in range(10):
    state = torch.rand((1, 1, 84, 84))
    action = i % 5
    reward = 0.01
    terminal = 0

    mem.append(state, action, reward, terminal)

# mem.transitions.data["reward"]
mem.transitions.data["reward"]

mem.sample(1)[6]
